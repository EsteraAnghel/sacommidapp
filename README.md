# README #

#####################################################################
Test Problem solution
Application :SacomMidApp
Created By : Estera Anghel
Email:estera.anghel1@gmail.com
Date:9.11.2020
Java:Simple App- clone and run it in IDE
#####################################################################
Shor Descripion
Library Used: DOM library in order to process(read/write) XML files.
Structure of the application :
3 Java Model Objects  :
	
	Product with attributes:
		private String description;
		private String gtin;
		private Price price;
		private String supplier;
		private String orderId;
		private Date orderCreated;
	
	Order with attributes:
		private Date creationDate;
		private String id;
		private List<Product> products;

	Price with attributes:
		private String currency;
		private double value;
		
1 Service: ProductService

2 Constants Classes : ProductConstants,OrderConstants

2 Classes for processing the XML file :ProcessXMLFile, WriteXMLFile

Application SacomMidApp read orders XML files from folder input, and write suppliers XML files in folder Output.
Processing input XML files we optain Java Objects:Orders with products.
On each product we have  2 additional attributes needed to create suppliers XML files (OrderCreated- the date when the order was placed, and orderId).
Then a List is Created with all products, and it will be sorded  using method sortProductsByDateAndPrice from ProducService class.
Then a Set<> is populated with all Suppliers, using the list with allProducts.
For each supplier the app  searched in list of allProducts for supliers's products and create the output XML file .
For Naming the suppliers file app use supplier name and the substring of the name of inputfile without word "order".
