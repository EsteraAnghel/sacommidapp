package model.constants;

public class ProductConstants {

    public static final String PRODUCT_DESCRIPTION = "description";
    public static final String PRODUCT_GTIN = "gtin";
    public static final String PRODUCT_PRICE = "price";
    public static final String PRODUCT_SUPPLIER = "supplier";
    public static final String PRICE_CURRENCY = "currency";

}
