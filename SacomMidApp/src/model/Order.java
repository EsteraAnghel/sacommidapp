package model;

import java.util.Date;
import java.util.List;

public class Order  implements Comparable<Order> {
    private Date creationDate;
    private String id;
    private List<Product> products;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    @Override
    public int compareTo(Order o) {
        return getCreationDate().compareTo(o.getCreationDate());
    }
}
