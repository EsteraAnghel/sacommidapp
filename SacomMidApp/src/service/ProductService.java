package service;

import model.Order;
import model.Price;
import model.Product;
import model.constants.ProductConstants;
import org.w3c.dom.Element;

import java.util.Comparator;
import java.util.List;

public class ProductService {
    public  void sortProductsByDateAndPrice(List<Product> list) {
        list.sort(
                Comparator.comparing(Product::getOrderCreated).thenComparing((a -> a.getPrice().getValue())).reversed()
        );
    }

    public  Product populateProduct(Order order, Element productElem) {
        Product product = new Product();
        Price price = new Price();
        product.setDescription(productElem.getElementsByTagName(ProductConstants.PRODUCT_DESCRIPTION).item(0).getTextContent());
        product.setGtin(productElem.getElementsByTagName(ProductConstants.PRODUCT_GTIN).item(0).getTextContent());
        price.setCurrency(productElem.getElementsByTagName(ProductConstants.PRODUCT_PRICE).item(0).getAttributes().getNamedItem(ProductConstants.PRICE_CURRENCY).getTextContent());
        price.setValue(Double.parseDouble(productElem.getElementsByTagName(ProductConstants.PRODUCT_PRICE).item(0).getTextContent()));
        product.setPrice(price);
        product.setSupplier(productElem.getElementsByTagName(ProductConstants.PRODUCT_SUPPLIER).item(0).getTextContent());
        product.setOrderId(order.getId());
        product.setOrderCreated(order.getCreationDate());
        return product;
    }

}
