import model.Product;
import model.constants.ProductConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

class WriteXMLFile {

    private  Document createDocument()
            throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document document = docBuilder.newDocument();
        document.setXmlStandalone(true);
        return document;
    }

    private Element addRootElement(Document products) {

        Element productEle = products.createElement("products");

        products.appendChild(productEle);
        return productEle;
    }

    private Element createProducts(Document plant, Product product) {
        Element book = plant.createElement("product");
        book.appendChild(createDescriptionNode(plant, product));
        book.appendChild(createGtinNode(plant, product));
        book.appendChild(createPriceNode(plant, product));
        book.appendChild(createOrderidNode(plant, product));
        return book;
    }

    private   Element createDescriptionNode(Document plant, Product product) {
        Element descriptionNode = plant.createElement(ProductConstants.PRODUCT_DESCRIPTION);
        Text description = plant.createTextNode(product.getDescription());
        descriptionNode.appendChild(description);
        return descriptionNode;
    }

    private  Element createGtinNode(Document plant, Product product) {
        Element gtinNode = plant.createElement(ProductConstants.PRODUCT_GTIN);
        Text gtin = plant.createTextNode(product.getGtin());
        gtinNode.appendChild(gtin);
        return gtinNode;
    }

    private  Element createOrderidNode(Document plant, Product product) {
        Element orderidNode = plant.createElement("orderid");
        Text orderid = plant.createTextNode(product.getOrderId());
        orderidNode.appendChild(orderid);
        return orderidNode;
    }

    private  Element createPriceNode(Document plant, Product product) {
        Element priceNode = plant.createElement(ProductConstants.PRODUCT_PRICE);
        priceNode.setAttribute(ProductConstants.PRICE_CURRENCY, product.getPrice().getCurrency());
        Text price = plant.createTextNode(String.valueOf(product.getPrice().getValue()));
        priceNode.appendChild(price);
        return priceNode;
    }

    private void writeToFile(Document plant, String no) throws Exception {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = transFactory.newTransformer();
        DOMSource source = new DOMSource(plant);
        File xmlFile = new File("..//SacomMidApp//output//" + no);
        StreamResult target = new StreamResult(xmlFile);
        transformer.transform(source, target);
    }

    public  void createsupplierXML(String nameOfFile, List<Product> list) throws Exception {
        Document plant = createDocument();

        if (!list.isEmpty()) {
            Element products = addRootElement(plant);
            for (Product product : list) {

                products.appendChild(createProducts(plant, product));
            }

            writeToFile(plant, nameOfFile);
        }
    }
}
