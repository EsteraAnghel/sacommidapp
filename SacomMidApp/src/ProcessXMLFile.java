import model.Order;
import model.Product;
import model.constants.OrderConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import service.ProductService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class ProcessXMLFile {
    private static final int NO_OF_CHAR_NOT_NEEDED = 5;

    public static void main(String argv[]) {
        WriteXMLFile writeXMLFile = new WriteXMLFile();
        ProductService productService = new ProductService();
        try {
//creating a constructor of file class and parsing an XML file
            File folder = new File("..//SacomMidApp//input");
            File[] listOfFiles = folder.listFiles();
            if (Objects.nonNull(listOfFiles)) {
                for (File file : listOfFiles) {
                    if (file.isFile()) {

//an instance of factory that gives a document builder
                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//an instance of builder to parse the specified xml file
                        DocumentBuilder db = dbf.newDocumentBuilder();
                        Document doc = db.parse(file);
                        doc.getDocumentElement().normalize();
                        NodeList nodeList = doc.getElementsByTagName("order");
// nodeList is not iterable, so we are using for loop
                        List<Order> orders = new ArrayList<>();
                        List<Product> allProducts = new ArrayList<>();

                        for (int temp = 0; temp < nodeList.getLength(); temp++) {
                            Node nNode = nodeList.item(temp);

                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;


                                Order order = new Order();
                                List<Product> products = new ArrayList<>();

                                order.setId(eElement.getAttribute(OrderConstants.ORDER_ID));
                                order.setCreationDate(paseXMLDate(eElement.getAttribute(OrderConstants.ORDER_CREATEION_DATE)));

                                NodeList productsList = eElement.getElementsByTagName("product");

                                for (int count = 0; count < productsList.getLength(); count++) {
                                    Node node1 = productsList.item(count);

                                    if (node1.getNodeType() == node1.ELEMENT_NODE) {
                                        Element productElem = (Element) node1;
                                        Product product = productService.populateProduct(order, productElem);
                                        //System.out.println(product.getDescription());
                                        products.add(product);
                                        allProducts.add(product);
                                    }
                                    order.setProducts(products);
                                    orders.add(order);
                                }
                            }

                            //......................................................
                            productService.sortProductsByDateAndPrice(allProducts);

                            Set<String> supl = getSuppliersSet(allProducts);
                            for (String suplier : supl) {
                                List<Product> productList = new ArrayList<>();
                                allProducts.stream().forEach(product -> {
                                    if (product.getSupplier().equals(suplier))
                                        productList.add(product);
                                });
                                String supplierFileName = suplier + file.getName().substring(NO_OF_CHAR_NOT_NEEDED);
                                writeXMLFile.createsupplierXML(supplierFileName, productList);

                            }
                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static Set<String> getSuppliersSet(List<Product> list) {
        return list.stream()
                .map(Product::getSupplier)
                .collect(Collectors.toSet());
    }


    private static Date paseXMLDate(String dateTime) {
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ss");
        Date date;
        try {
            date = parser.parse(dateTime);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
